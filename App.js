import React from 'react';

import RouterComponent from './src/navigation/routerComponent';

const App = () => {
  return <RouterComponent />;
};

export default App;
