import React from 'react';
import {Button, View} from 'react-native';

export const HomeScreen = ({navigation}) => {
  return (
    <View>
      <Button
        title="Facbook SignIn"
        onPress={() => navigation.navigate('Main')}
      />
    </View>
  );
};
